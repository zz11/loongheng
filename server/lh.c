#include "lh.h"
//#define ip "192.168.4.250"
#define ip "10.1.6.104"
#define port 1111

int main(int argc,int *argv[])
{
 int ret;
 struct mpu_data data[10];
 struct new_data newdata={
				.ax=0,
        .ay=0,
        .az=0,
        .gx=0,
        .gy=0,
        .gz=0,
};

 signal(SIGPIPE,SIG_IGN);

 mpu_fd=open("/dev/mpu6050",O_RDWR);
 if(mpu_fd<0)
 {
   printf("open mpu 6050 error\n");
   return -1;
 }

 moto_fd=open("/dev/tb6612",O_RDWR);
 if(moto_fd<0)
 {
   printf("open tb6612 error\n");
   return -1;
 }


 //连接网络调试端
 ret=connect_debug(ip,port);
  
  //程序主循环
  while(1)
  {
  ret=read_mpu(data);//读取原生数据
  ret=make_data(data,&newdata);//对数据滤波并产生角度及角数度

  //pid计算
  newdata.pwmL = 3.8 * newdata.ang_ax;
  newdata.pwmR = 3.8 * newdata.ang_ax;  

  //调整姿态

  //左轮姿态
  if(newdata.pwmL >=0 )
  {
    ioctl(moto_fd,0,(int)(newdata.pwmL*100*1000));
  }
   else
  {
    ioctl(moto_fd,1,(int)(abs(newdata.pwmL)*100*1000));
  }

  //右轮姿态
  if(newdata.pwmR >=0 )
  {
    ioctl(moto_fd,3,(int)(newdata.pwmR*100*1000));
  }
   else
  {
    ioctl(moto_fd,2,(int)(abs(newdata.pwmR)*100*1000));
  }
  

  ret=send_debug(&newdata);  //发送调试数据
  if(ret<=0)connect_debug(ip,port);//断线重连
  //printf("x:%lf y:%lf z:%lf \n",newdata.raw_ax,newdata.raw_ay,newdata.raw_az);

  }//主循环结束
 return 0;
}


