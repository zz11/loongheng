#include "lh.h"

int read_mpu(struct mpu_data *data)
{
 int i,ret;
  for(i=0;i<10;i++)
   {
    ret=read(mpu_fd,data+i,12);
    if (ret<12)perror("read mpu error");
   }
 return 0;
}


int make_data(struct mpu_data *data,struct new_data *newdata)
{
  int i;
  memset(newdata,0,sizeof(struct new_data));
  //填写原生数据
    newdata->raw_ax=(float)((short int)((data[0].raw_axh<<8)+data[0].raw_axl))/16384.00;
    newdata->raw_ay=(float)((short int)((data[0].raw_ayh<<8)+data[0].raw_ayl))/16384.00;
    newdata->raw_az=(float)((short int)((data[0].raw_azh<<8)+data[0].raw_azl))/16384.00;
    newdata->raw_gx=(float)((short int)((data[0].raw_gxh<<8)+data[0].raw_gxl))/32.8;
    newdata->raw_gy=(float)((short int)((data[0].raw_gyh<<8)+data[0].raw_gyl))/32.8;
    newdata->raw_gz=(float)((short int)((data[0].raw_gzh<<8)+data[0].raw_gzl))/32.8;

 //计算平均数据
  for(i=0;i<10;i++)
  {
    newdata->ax+=(short int)((data[i].raw_axh<<8)+data[i].raw_axl);
    newdata->ay+=(short int)((data[i].raw_ayh<<8)+data[i].raw_ayl);
    newdata->az+=(short int)((data[i].raw_azh<<8)+data[i].raw_azl);
    newdata->gx+=(short int)((data[i].raw_gxh<<8)+data[i].raw_gxl);
    newdata->gy+=(short int)((data[i].raw_gyh<<8)+data[i].raw_gyl);
    newdata->gz+=(short int)((data[i].raw_gzh<<8)+data[i].raw_gzl);
  }
    newdata->ax = newdata->ax/10.0;
    newdata->ay = newdata->ay/10.0;
    newdata->az = newdata->az/10.0;
    newdata->gx = newdata->gx/10.0;
    newdata->gy = newdata->gy/10.0;
    newdata->gz = newdata->gz/10.0;

//把加速度计算成角度(只对加速度计做计算)
   
   //处理原生数据角度
   
   //x轴
   newdata->ang_rax=newdata->raw_ax/sqrt((newdata->raw_ay*newdata->raw_ay +  
                                          newdata->raw_az*newdata->raw_az));
   newdata->ang_rax=atan(newdata->ang_rax)*180.0/3.14;
   //y轴
   newdata->ang_ray=newdata->raw_ay/sqrt((newdata->raw_ax*newdata->raw_ax +  
                                          newdata->raw_az*newdata->raw_az));
   newdata->ang_ray=atan(newdata->ang_ray)*180.0/3.14;
   //z轴
   newdata->ang_raz=sqrt((newdata->raw_ax*newdata->raw_ax +  
                         newdata->raw_ay*newdata->raw_ay))/newdata->raw_az;
   newdata->ang_raz=atan(newdata->ang_raz)*180.0/3.14;

   //处理平均数据角度
   
   //x轴
   newdata->ang_ax=newdata->ax/sqrt((newdata->ay*newdata->ay +  
                                          newdata->az*newdata->az));
   newdata->ang_ax=atan(newdata->ang_ax)*180.0/3.14;
   //y轴
   newdata->ang_ay=newdata->ay/sqrt((newdata->ax*newdata->ax +  
                                          newdata->az*newdata->az));
   newdata->ang_ay=atan(newdata->ang_ay)*180.0/3.14;
   //z轴
   newdata->ang_az=sqrt((newdata->ax*newdata->ax +  
                         newdata->ay*newdata->ay))/newdata->az;
   newdata->ang_az=atan(newdata->ang_az)*180.0/3.14;
  return 0;
}
