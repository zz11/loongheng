#include "lh.h"


int connect_debug(char ipaddr[15],int port)
{
 int ret,i;
 struct sockaddr_in addr;
  sock_fd=socket(AF_INET,SOCK_STREAM,0);
  addr.sin_family = AF_INET;
  addr.sin_port=htons(port);
  addr.sin_addr.s_addr=inet_addr(ipaddr);
 
 ret=connect(sock_fd,(struct sockaddr *)&addr,sizeof(addr));  
 if(ret==0)
 {
  sock_stat=1; 
 }
 else
 {
   close(sock_fd);
 }
return 0;
}

int send_debug(struct new_data *data)
{
  int ret=0;
  char temp;
  if(sock_stat==1)
  {
   ret=send(sock_fd,data,sizeof(struct new_data),0);
   if(ret<=0)
     {
       sock_stat=0;
       close(sock_fd);
     } 
  }
  return ret;
}
