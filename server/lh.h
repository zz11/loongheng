#ifndef lh_h
#define lh_h
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

int moto_fd,mpu_fd,sock_fd,sock_stat;

struct mpu_data{
       unsigned char raw_axh;
       unsigned char raw_axl;
       unsigned char raw_ayh;
       unsigned char raw_ayl;
       unsigned char raw_azh;
       unsigned char raw_azl;
     
       unsigned char raw_gxh;
       unsigned char raw_gxl;
       unsigned char raw_gyh;
       unsigned char raw_gyl;
       unsigned char raw_gzh;
       unsigned char raw_gzl;
};

struct new_data{
       //原生数据得到的加速度数据
       float raw_ax;
       float raw_ay;
       float raw_az;
       float raw_gx;
       float raw_gy;
       float raw_gz; 

       //原生数据得到的角度数据
       float ang_rax;
       float ang_ray;
       float ang_raz;
       float ang_rgx;
       float ang_rgy;
       float ang_rgz; 

       //平均加速度数据
       float ax;
       float ay;
       float az;
       float gx;
       float gy;
       float gz; 

      
       //滤波后得到的角度数据
       float ang_ax;
       float ang_ay;
       float ang_az;
       float ang_gx;
       float ang_gy;
       float ang_gz;

       float pwmL;
       float pwmR; 
};
//读取mpu数据
int read_mpu(struct mpu_data *);
//处理mpu数据到new_data
int make_data(struct mpu_data *,struct new_data *);
//tcp连接到远程调试端
int connect_debug(char ipaddr[15],int port);//tcp发送调试数据
int send_debug(struct new_data *);

#endif
