#-------------------------------------------------
#
# Project created by QtCreator 2015-07-31T17:16:02
#
#-------------------------------------------------

QT       += core gui network printsupport opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lh-debug
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h

FORMS    += mainwindow.ui

CONFIG += mobility
MOBILITY = 

