#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QGLWidget>

namespace Ui {
class MainWindow;
}
struct new_data{
        //原生数据得到的加速度数据
        float raw_ax;
        float raw_ay;
        float raw_az;
        float raw_gx;
        float raw_gy;
        float raw_gz;

       //原生数据得到的角度数据
        float ang_rax;
        float ang_ray;
        float ang_raz;
        float ang_rgx;
        float ang_rgy;
        float ang_rgz;

        //平均加速度数据
        float ax;
        float ay;
        float az;
        float gx;
        float gy;
        float gz;

        //滤波后得到的角度数据
        float ang_ax;
        float ang_ay;
        float ang_az;
        float ang_gx;
        float ang_gy;
        float ang_gz;

        float pwmL;
        float pwmR;
       };

 static QVector<double> d_rax(200), //raw data
                        d_ray(200),
                        d_raz(200),

                        d_arax(200), //acc raw angle data
                        d_aray(200),
                        d_araz(200),

                        d_ang_ax(200), // filter angle data
                        d_ang_ay(200),
                        d_ang_az(200),

                        d_rgx(200),  //g raw data
                        d_rgy(200),
                        d_rgz(200),

                        d_pwmL(200), //moto pwm
                        d_pwmR(200),

                        timex(200);
 static long long dtime;

 class glwidget : public QGLWidget
 {
     Q_OBJECT
    public :
    glwidget(QWidget *parent=0);
    void initializeGL();
    void paintGL();
    //void resizeGL(int w,int h);
 };



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QTcpServer *server;
    QTcpSocket *sock;
private:
    Ui::MainWindow *ui;
    struct new_data mpudata;
    glwidget *gl_win;
private slots:
    void con_data();
    void read_data();
    void discon_data();
};




#endif // MAINWINDOW_H
