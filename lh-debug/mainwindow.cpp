#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtNetwork>
#include <QDebug>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    this->setGeometry(0,0,1024,768);// set window size
    int bar_h=this->height()/4;
    dtime=0;
    this->setWindowTitle("龙衡-两轮平衡车调试工具 Power by zz Email:90807@QQ.COM");

    //set all diagram
    ui->acc_xyz->setGeometry(0,0,this->width()/5*2,bar_h);
    ui->g_xyz->setGeometry(width()/5*2,0,this->width()/5*2,bar_h);

    ui->acc_angx->setGeometry(0,bar_h,this->width()/3,bar_h);
    ui->acc_angy->setGeometry(width()/3,bar_h,this->width()/3,bar_h);
    ui->acc_angz->setGeometry(this->width()/3*2,bar_h,this->width()/3,bar_h);

    ui->moto_pwm->setGeometry(0,bar_h*2,this->width()/3,bar_h);

    ui->ValueBox->setGeometry(0,bar_h*3,this->width(),bar_h);

    //set opengl
    ui->gl_win->setGeometry(this->width()/5*4,0,this->width()/5+3,bar_h);
    gl_win = new glwidget(this);
    gl_win->initializeGL();
    gl_win->paintGL();
    gl_win->show();

    //set acc xyz background
    ui->acc_xyz->setBackground(Qt::black);
    ui->acc_xyz->setLocale(QLocale::Chinese);
    ui->acc_xyz->xAxis->setRange(0.0,200.00);
    ui->acc_xyz->yAxis->setRange(-1.6,1.6);
    ui->acc_xyz->xAxis->setBasePen(QPen(Qt::green));
    ui->acc_xyz->yAxis->setLabelColor(Qt::green);
    ui->acc_xyz->yAxis->setLabel("加速度RAW数据");
    ui->acc_xyz->xAxis->setSubTickPen(QPen(Qt::green));
    ui->acc_xyz->xAxis->setTickLabelColor(Qt::green);
    ui->acc_xyz->xAxis->setTickPen(QPen(Qt::green));
    ui->acc_xyz->yAxis->setBasePen(QPen(Qt::green));
    ui->acc_xyz->yAxis->setLabelColor(Qt::green);
    ui->acc_xyz->yAxis->setSubTickPen(QPen(Qt::green));
    ui->acc_xyz->yAxis->setTickLabelColor(Qt::green);
    ui->acc_xyz->yAxis->setTickPen(QPen(Qt::green));

    ui->acc_xyz->addGraph();
    ui->acc_xyz->addGraph();
    ui->acc_xyz->addGraph();
    ui->acc_xyz->graph(0)->setPen(QPen(Qt::cyan,3)); //x
    ui->acc_xyz->graph(1)->setPen(QPen(Qt::yellow,3)); //y
    ui->acc_xyz->graph(2)->setPen(QPen(Qt::magenta,3)); //z

    //set g xyz background
    ui->g_xyz->setBackground(Qt::black);
    ui->g_xyz->setLocale(QLocale::Chinese);
    ui->g_xyz->xAxis->setRange(0.0,200.00);
    ui->g_xyz->yAxis->setRange(-1100,1100);
    ui->g_xyz->xAxis->setBasePen(QPen(Qt::green));
    ui->g_xyz->yAxis->setLabelColor(Qt::green);
    ui->g_xyz->yAxis->setLabel("陀螺仪RAW数据");
    ui->g_xyz->xAxis->setSubTickPen(QPen(Qt::green));
    ui->g_xyz->xAxis->setTickLabelColor(Qt::green);
    ui->g_xyz->xAxis->setTickPen(QPen(Qt::green));
    ui->g_xyz->yAxis->setBasePen(QPen(Qt::green));
    ui->g_xyz->yAxis->setLabelColor(Qt::green);
    ui->g_xyz->yAxis->setSubTickPen(QPen(Qt::green));
    ui->g_xyz->yAxis->setTickLabelColor(Qt::green);
    ui->g_xyz->yAxis->setTickPen(QPen(Qt::green));

    ui->g_xyz->addGraph();
    ui->g_xyz->addGraph();
    ui->g_xyz->addGraph();
    ui->g_xyz->graph(0)->setPen(QPen(Qt::cyan,3)); //x
    ui->g_xyz->graph(1)->setPen(QPen(Qt::yellow,3)); //y
    ui->g_xyz->graph(2)->setPen(QPen(Qt::magenta,3)); //z

    //set ang x background
    ui->acc_angx->setBackground(Qt::black);
    ui->acc_angx->xAxis->setRange(0.0,200.00);
    ui->acc_angx->yAxis->setRange(-90,90);
    ui->acc_angx->setLocale(QLocale::Chinese);
    ui->acc_angx->xAxis->setBasePen(QPen(Qt::green));
    ui->acc_angx->yAxis->setLabelColor(Qt::green);
    ui->acc_angx->yAxis->setLabel("X轴角度");
    ui->acc_angx->xAxis->setSubTickPen(QPen(Qt::green));
    ui->acc_angx->xAxis->setTickLabelColor(Qt::green);
    ui->acc_angx->xAxis->setTickPen(QPen(Qt::green));
    ui->acc_angx->yAxis->setBasePen(QPen(Qt::green));
    ui->acc_angx->yAxis->setLabelColor(Qt::green);
    ui->acc_angx->yAxis->setSubTickPen(QPen(Qt::green));
    ui->acc_angx->yAxis->setTickLabelColor(Qt::green);
    ui->acc_angx->yAxis->setTickPen(QPen(Qt::green));
    ui->acc_angx->addGraph();
    ui->acc_angx->addGraph();
    ui->acc_angx->graph(0)->setPen(QPen(Qt::cyan,5)); //x
    ui->acc_angx->graph(1)->setPen(QPen(Qt::magenta,3)); //y

    //set ang y background
    ui->acc_angy->setBackground(Qt::black);
    ui->acc_angy->xAxis->setRange(0.0,200.00);
    ui->acc_angy->yAxis->setRange(-90,90);
    ui->acc_angy->setLocale(QLocale::Chinese);
    ui->acc_angy->xAxis->setBasePen(QPen(Qt::green));
    ui->acc_angy->yAxis->setLabelColor(Qt::green);
    ui->acc_angy->yAxis->setLabel("Y轴角度");
    ui->acc_angy->xAxis->setSubTickPen(QPen(Qt::green));
    ui->acc_angy->xAxis->setTickLabelColor(Qt::green);
    ui->acc_angy->xAxis->setTickPen(QPen(Qt::green));
    ui->acc_angy->yAxis->setBasePen(QPen(Qt::green));
    ui->acc_angy->yAxis->setLabelColor(Qt::green);
    ui->acc_angy->yAxis->setSubTickPen(QPen(Qt::green));
    ui->acc_angy->yAxis->setTickLabelColor(Qt::green);
    ui->acc_angy->yAxis->setTickPen(QPen(Qt::green));
    ui->acc_angy->addGraph();
    ui->acc_angy->addGraph();
    ui->acc_angy->graph(0)->setPen(QPen(Qt::cyan,5)); //x
    ui->acc_angy->graph(1)->setPen(QPen(Qt::magenta,3)); //y

    //set ang x background
    ui->acc_angz->setBackground(Qt::black);
    ui->acc_angz->xAxis->setRange(0.0,200.00);
    ui->acc_angz->yAxis->setRange(-90,90);
    ui->acc_angz->setLocale(QLocale::Chinese);
    ui->acc_angz->xAxis->setBasePen(QPen(Qt::green));
    ui->acc_angz->yAxis->setLabelColor(Qt::green);
    ui->acc_angz->yAxis->setLabel("Z轴角度");
    ui->acc_angz->xAxis->setSubTickPen(QPen(Qt::green));
    ui->acc_angz->xAxis->setTickLabelColor(Qt::green);
    ui->acc_angz->xAxis->setTickPen(QPen(Qt::green));
    ui->acc_angz->yAxis->setBasePen(QPen(Qt::green));
    ui->acc_angz->yAxis->setLabelColor(Qt::green);
    ui->acc_angz->yAxis->setSubTickPen(QPen(Qt::green));
    ui->acc_angz->yAxis->setTickLabelColor(Qt::green);
    ui->acc_angz->yAxis->setTickPen(QPen(Qt::green));
    ui->acc_angz->addGraph();
    ui->acc_angz->addGraph();
    ui->acc_angz->graph(0)->setPen(QPen(Qt::cyan,5)); //x
    ui->acc_angz->graph(1)->setPen(QPen(Qt::magenta,3)); //y

    //set moto pwm background
    ui->moto_pwm->setBackground(Qt::black);
    ui->moto_pwm->xAxis->setRange(0.0,200.00);
    ui->moto_pwm->yAxis->setRange(-90,90);
    ui->moto_pwm->setLocale(QLocale::Chinese);
    ui->moto_pwm->xAxis->setBasePen(QPen(Qt::green));
    ui->moto_pwm->yAxis->setLabelColor(Qt::green);
    ui->moto_pwm->yAxis->setLabel("PWM");
    ui->moto_pwm->xAxis->setSubTickPen(QPen(Qt::green));
    ui->moto_pwm->xAxis->setTickLabelColor(Qt::green);
    ui->moto_pwm->xAxis->setTickPen(QPen(Qt::green));
    ui->moto_pwm->yAxis->setBasePen(QPen(Qt::green));
    ui->moto_pwm->yAxis->setLabelColor(Qt::green);
    ui->moto_pwm->yAxis->setSubTickPen(QPen(Qt::green));
    ui->moto_pwm->yAxis->setTickLabelColor(Qt::green);
    ui->moto_pwm->yAxis->setTickPen(QPen(Qt::green));
    ui->moto_pwm->addGraph();
    ui->moto_pwm->addGraph();
    ui->moto_pwm->graph(0)->setPen(QPen(Qt::cyan,5)); //x
    ui->moto_pwm->graph(1)->setPen(QPen(Qt::magenta,3)); //y


    //init network
    server= new QTcpServer(this);
    server->listen(QHostAddress::Any,1111);
    connect(server,SIGNAL(newConnection()),this,SLOT(con_data()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete server;
    delete sock;
}

void MainWindow::con_data()
{

   while(server->hasPendingConnections())
   {
     sock=server->nextPendingConnection();
     connect(sock,SIGNAL(readyRead()),this,SLOT(read_data()));
     connect(sock,SIGNAL(disconnected()),this,SLOT(discon_data()));
     qDebug()<<sock->peerAddress().toString();
   }

}

void MainWindow::discon_data()
{
  qDebug()<<"disconnect";
}

void MainWindow::read_data()
{
  QByteArray str;
  str=sock->readAll();
  char *tmp = str.data();
  memcpy(&mpudata,tmp,sizeof(struct new_data));

  ////////////////////reset diagram and all data ////////////////////////////
  if(dtime>1000000)
  {
      dtime=0;
      d_rax.clear();
      d_ray.clear();
      d_raz.clear();
      d_arax.clear();
      d_aray.clear();
      d_araz.clear();
      d_ang_ax.clear();
      d_ang_ay.clear();
      d_ang_az.clear();
      d_rgx.clear();
      d_rgy.clear();
      d_rgz.clear();
      d_pwmL.clear();
      d_pwmR.clear();
      timex.clear();
      ui->acc_xyz->xAxis->setRange(0,200);
      ui->g_xyz->xAxis->setRange(0,200);
      ui->acc_angx->xAxis->setRange(0,200);
      ui->acc_angy->xAxis->setRange(0,200);
      ui->acc_angz->xAxis->setRange(0,200);
      ui->moto_pwm->xAxis->setRange(0,200);
  }
//////////////////////////////////////////////////////////////////////////////

///////append data in qvertor////////////////////////////////////////
  timex.append(dtime++);  //times add

  d_rax.append(mpudata.raw_ax); //append raw data
  d_ray.append(mpudata.raw_ay);
  d_raz.append(mpudata.raw_az);

  d_arax.append(mpudata.ang_rax); //append raw angle
  d_aray.append(mpudata.ang_ray);
  d_araz.append(mpudata.ang_raz);

  d_ang_ax.append(mpudata.ang_ax); //filter angle
  d_ang_ay.append(mpudata.ang_ay);
  d_ang_az.append(mpudata.ang_az);

  d_rgx.append(mpudata.raw_gx); //append raw g data
  d_rgy.append(mpudata.raw_gy);
  d_rgz.append(mpudata.raw_gz);

  d_pwmL.append(mpudata.pwmL); //moto pwm
  d_pwmR.append(mpudata.pwmR);

 ////////update label in mainwindow /////////////////////////////
  ui->lab_rax->setText(QString("RAW_ax:  %1").arg(mpudata.raw_ax));
  ui->lab_ray->setText(QString("RAW_ay:  %1").arg(mpudata.raw_ay));
  ui->lab_raz->setText(QString("RAW_az:  %1").arg(mpudata.raw_az));
  ui->lab_rgx->setText(QString("RAW_gx:  %1").arg(mpudata.raw_gx));
  ui->lab_rgy->setText(QString("RAW_gy:  %1").arg(mpudata.raw_gy));
  ui->lab_rgz->setText(QString("RAW_gz:  %1").arg(mpudata.raw_gz));

  ui->lab_rang_x->setText(QString("rAngle_x:  %1").arg(mpudata.ang_rax));
  ui->lab_rang_y->setText(QString("rAngle_y:  %1").arg(mpudata.ang_ray));
  ui->lab_rang_z->setText(QString("rAngle_z:  %1").arg(mpudata.ang_az));
  ui->lab_ang_x->setText(QString("Angle_x:  %1").arg(mpudata.ang_ax));
  ui->lab_ang_y->setText(QString("Angle_y:  %1").arg(mpudata.ang_ay));
  ui->lab_ang_z->setText(QString("Angle_z:  %1").arg(mpudata.ang_az));

  ui->lab_pwml->setText(QString("PWM_L:  %1").arg(mpudata.pwmL));
  ui->lab_pwmr->setText(QString("PWM_R:  %1").arg(mpudata.pwmR));

///////erase data in qvertor & set diagram range //////////////////////
  if(dtime>190)
  {
     ui->acc_xyz->xAxis->setRange(dtime-190,dtime+10);
     ui->g_xyz->xAxis->setRange(dtime-190,dtime+10);
     ui->acc_angx->xAxis->setRange(dtime-190,dtime+10);
     ui->acc_angy->xAxis->setRange(dtime-190,dtime+10);
     ui->acc_angz->xAxis->setRange(dtime-190,dtime+10);
     ui->moto_pwm->xAxis->setRange(dtime-190,dtime+10);

     d_rax.erase(d_rax.begin());
     d_ray.erase(d_ray.begin());
     d_raz.erase(d_raz.begin());

     d_arax.erase(d_arax.begin());
     d_aray.erase(d_aray.begin());
     d_araz.erase(d_araz.begin());

     d_ang_ax.erase(d_ang_ax.begin());
     d_ang_ay.erase(d_ang_ay.begin());
     d_ang_az.erase(d_ang_az.begin());

     d_rgx.erase(d_rgx.begin());
     d_rgy.erase(d_rgy.begin());
     d_rgz.erase(d_rgz.begin());

     d_pwmL.erase(d_pwmL.begin());
     d_pwmR.erase(d_pwmR.begin());

     timex.erase(timex.begin());
  }

///////draw diagran graph////////////////////////////////////////////////
  ui->acc_xyz->graph(0)->setData(timex,d_rax);
  ui->acc_xyz->graph(1)->setData(timex,d_ray);
  ui->acc_xyz->graph(2)->setData(timex,d_raz);
  ui->acc_xyz->replot();

  ui->g_xyz->graph(0)->setData(timex,d_rgx);
  ui->g_xyz->graph(1)->setData(timex,d_rgy);
  ui->g_xyz->graph(2)->setData(timex,d_rgz);
  ui->g_xyz->replot();

  ui->acc_angx->graph(0)->setData(timex,d_arax);
  ui->acc_angx->graph(1)->setData(timex,d_ang_ax);
  ui->acc_angx->replot();

  ui->acc_angy->graph(0)->setData(timex,d_aray);
  ui->acc_angy->graph(1)->setData(timex,d_ang_ay);
  ui->acc_angy->replot();

  ui->acc_angz->graph(0)->setData(timex,d_arax);
  ui->acc_angz->graph(1)->setData(timex,d_ang_az);
  ui->acc_angz->replot();


  ui->moto_pwm->graph(0)->setData(timex,d_pwmL);
  ui->moto_pwm->graph(1)->setData(timex,d_pwmR);
  ui->moto_pwm->replot();

}
glwidget::glwidget(QWidget * parent):QGLWidget(parent)
{

}
void glwidget::initializeGL()
{
  glClearColor(0.0,0.2,1.0,1.0);
  glShadeModel(GL_SMOOTH);
  glEnable(GL_DEPTH);
  setGeometry(805,5,210,186);
}

void glwidget::paintGL()
{
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();

  glBegin(GL_TRIANGLES);
      glVertex3f(0.0,1.0,0.0);
      glVertex3f(-1.0,-1.0,0.0);
      glVertex3f(1.0,-1.0,0.0);
  glEnd();
  glTranslatef(3.0,0.0,0.0);
}
