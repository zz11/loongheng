//////////////////////////////////////////////
// 龙衡平衡小车mpu6050 9轴模块驱动
// 作者：zz                               
// Email:90807@QQ.com
// 本软件免费开源，修改请保留作者信息，谢谢
/////////////////////////////////////////////

#include <linux/init.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>

struct class *cls;

//设备结构体
struct i2c_client *client;
struct i2c_board_info info;
static unsigned short addrs[]={(0x68),I2C_CLIENT_END};

struct mpu6050data{
							unsigned char acc_xh;
              unsigned char acc_xl;
							unsigned char acc_yh;
							unsigned char acc_yl;
							unsigned char acc_zh;
							unsigned char acc_zl;
							unsigned char g_xh;
              unsigned char g_xl;
							unsigned char g_yh;
							unsigned char g_yl;
							unsigned char g_zh;
							unsigned char g_zl;
};

int read_i2c(struct mpu6050data *);

ssize_t open(struct inode *ind,struct file *fl)
{
  return 0;
}

ssize_t read(struct file *fl,char __user *buf,size_t len,loff_t *off)
{
  struct mpu6050data data;
  //int i=0,x,y,z,gx,gy,gz;
  unsigned char data_int=0;

  memset(&data,0,sizeof(data));

   //判断中断
  while(data_int!=1)
  {
    data_int=i2c_smbus_read_byte_data(client,0x3a);
  }
      data.acc_xh=i2c_smbus_read_byte_data(client,0x3b);
  		data.acc_xl=i2c_smbus_read_byte_data(client,0x3c);
  //x=(data.acc_xh<<8)|data.acc_xl;

  		data.acc_yh=i2c_smbus_read_byte_data(client,0x3d);
  		data.acc_yl=i2c_smbus_read_byte_data(client,0x3e); 
  //y=(data.acc_yh<<8)|data.acc_yl;

  		data.acc_zh=i2c_smbus_read_byte_data(client,0x3f);
  		data.acc_zl=i2c_smbus_read_byte_data(client,0x40);
  //z=(data.acc_zh<<8)|data.acc_zl;

  		data.g_xh=i2c_smbus_read_byte_data(client,0x43);
  		data.g_xl=i2c_smbus_read_byte_data(client,0x44);
  //gx=(data.g_xh<<8)|data.g_xl;

  		data.g_yh=i2c_smbus_read_byte_data(client,0x45);
  		data.g_yl=i2c_smbus_read_byte_data(client,0x46); 
  //gy=(data.g_yh<<8)|data.g_yl;

  		data.g_zh=i2c_smbus_read_byte_data(client,0x47);
  		data.g_zl=i2c_smbus_read_byte_data(client,0x48);
  //gz=(data.g_zh<<8)|data.g_zl;
   
  memcpy(buf,(char *)&data,12);
//  printk("ok %x\n",data.acc_xh);
  return 12;
}

struct file_operations fops={
				.owner=THIS_MODULE,
        .read=read,
        .open=open,
};

int probe(struct i2c_client *client,const struct i2c_device_id *id)
{
  int ret;
  unsigned char whoami,stat;
  ret=register_chrdev(233,"mpu6050",&fops);
  cls=class_create(THIS_MODULE,"mpu6050_cls");
  device_create(cls,NULL,MKDEV(233,0),NULL,"mpu6050"); //创建设备节点
  printk("mpu 6050  probe...\n");
  printk("Power by ZZ\n");
  printk("Email:90807@QQ.COM\n");
  
  i2c_smbus_write_byte_data(client,0x1b,1<<4);//配置陀螺仪量程+-1000
  i2c_smbus_write_byte_data(client,0x1c,0x00);//配置加速度计量程+-2g
  i2c_smbus_write_byte_data(client,0x6b,0x00);//唤醒mpu6050进入工作状态
  stat=i2c_smbus_read_byte_data(client,0x6b);
  whoami=i2c_smbus_read_byte_data(client,0x75);
  printk("ZZ:Init MPU6050 in addr 0x%x stat:%x\n",whoami,stat);
  return ret;
}

int read_i2c(struct mpu6050data *data)
{
  unsigned char get_acc[]={0x3b};
  struct i2c_msg cmd[2];
  cmd[0].addr = client->addr;
  cmd[0].flags=0; //write
  cmd[0].len=1;
  cmd[0].buf=get_acc;
  i2c_transfer(client->adapter,&cmd[0],1);
  mdelay(100);
 
  //cmd 1 为读数据
  cmd[1].addr = client->addr;
  cmd[1].flags =1;
  cmd[1].len=6;
  cmd[1].buf=(unsigned char *)data;
  i2c_transfer(client->adapter,&cmd[1],1);
  
  return 0;
}
  


int remove(struct i2c_client *client)
{
  printk("zz : mpu6050 remove...\n");
  device_destroy(cls,MKDEV(233,0));
  class_destroy(cls);
  unregister_chrdev(233,"mpu6050");
  return 0;
}

struct i2c_device_id ids[]={
				{"mpu6050"},
        {},
};

//驱动结构体
struct i2c_driver mpu6050_drv={
        .probe=probe,
        .remove=remove,
        .id_table=ids,
        .driver={
           .name="mpu6050",
       },
};


static int __init mpu6050_init(void)
{
  struct i2c_adapter *adapter;

  memset(&info,0,sizeof(info));
  strcpy(info.type,"mpu6050");
  adapter = i2c_get_adapter(0);
  client = i2c_new_probed_device(adapter,&info,addrs,NULL);
  i2c_put_adapter(adapter);
  return i2c_add_driver(&mpu6050_drv);
}

static void __exit mpu6050_exit(void)
{
   i2c_del_driver(&mpu6050_drv);
   i2c_unregister_device(client);
}


module_init(mpu6050_init);
module_exit(mpu6050_exit);
MODULE_LICENSE("GPL");
