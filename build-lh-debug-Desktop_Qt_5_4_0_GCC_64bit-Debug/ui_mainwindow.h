/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QOpenGLWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QCustomPlot *acc_xyz;
    QCustomPlot *acc_angx;
    QCustomPlot *acc_angy;
    QCustomPlot *acc_angz;
    QOpenGLWidget *gl_win;
    QCustomPlot *g_xyz;
    QCustomPlot *moto_pwm;
    QGroupBox *ValueBox;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QLabel *lab_rax;
    QLabel *lab_ray;
    QLabel *lab_raz;
    QLabel *lab_rgx;
    QLabel *lab_rgy;
    QLabel *lab_rgz;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QLabel *lab_rang_x;
    QLabel *lab_rang_y;
    QLabel *lab_rang_z;
    QLabel *lab_ang_x;
    QLabel *lab_ang_y;
    QLabel *lab_ang_z;
    QWidget *layoutWidget_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *lab_pwml;
    QLabel *lab_pwmr;
    QLabel *lab_rang_z_2;
    QLabel *lab_null;
    QLabel *lab_ang_y_2;
    QLabel *lab_ang_z_2;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1024, 768);
        MainWindow->setMinimumSize(QSize(1024, 768));
        MainWindow->setMaximumSize(QSize(1024, 768));
        MainWindow->setStyleSheet(QStringLiteral("background-color: rgb(39, 38, 38);"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        acc_xyz = new QCustomPlot(centralWidget);
        acc_xyz->setObjectName(QStringLiteral("acc_xyz"));
        acc_xyz->setGeometry(QRect(0, 0, 500, 150));
        acc_xyz->setMinimumSize(QSize(0, 0));
        acc_xyz->setMaximumSize(QSize(1024, 768));
        QPalette palette;
        QBrush brush(QColor(0, 255, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(0, 170, 255, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        QBrush brush2(QColor(190, 190, 190, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        acc_xyz->setPalette(palette);
        acc_xyz->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
        acc_angx = new QCustomPlot(centralWidget);
        acc_angx->setObjectName(QStringLiteral("acc_angx"));
        acc_angx->setGeometry(QRect(0, 300, 281, 150));
        acc_angx->setMinimumSize(QSize(0, 0));
        acc_angx->setMaximumSize(QSize(1024, 768));
        acc_angx->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
        acc_angy = new QCustomPlot(centralWidget);
        acc_angy->setObjectName(QStringLiteral("acc_angy"));
        acc_angy->setGeometry(QRect(280, 300, 281, 150));
        acc_angy->setMinimumSize(QSize(0, 0));
        acc_angy->setMaximumSize(QSize(1024, 768));
        acc_angy->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
        acc_angz = new QCustomPlot(centralWidget);
        acc_angz->setObjectName(QStringLiteral("acc_angz"));
        acc_angz->setGeometry(QRect(560, 300, 241, 150));
        acc_angz->setMinimumSize(QSize(0, 0));
        acc_angz->setMaximumSize(QSize(1024, 768));
        acc_angz->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
        gl_win = new QOpenGLWidget(centralWidget);
        gl_win->setObjectName(QStringLiteral("gl_win"));
        gl_win->setGeometry(QRect(590, 0, 300, 151));
        g_xyz = new QCustomPlot(centralWidget);
        g_xyz->setObjectName(QStringLiteral("g_xyz"));
        g_xyz->setGeometry(QRect(0, 150, 500, 150));
        g_xyz->setMinimumSize(QSize(0, 0));
        g_xyz->setMaximumSize(QSize(1024, 768));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        g_xyz->setPalette(palette1);
        g_xyz->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
        moto_pwm = new QCustomPlot(centralWidget);
        moto_pwm->setObjectName(QStringLiteral("moto_pwm"));
        moto_pwm->setGeometry(QRect(0, 450, 241, 150));
        moto_pwm->setMinimumSize(QSize(0, 0));
        moto_pwm->setMaximumSize(QSize(1024, 768));
        moto_pwm->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
        ValueBox = new QGroupBox(centralWidget);
        ValueBox->setObjectName(QStringLiteral("ValueBox"));
        ValueBox->setGeometry(QRect(0, 600, 1021, 171));
        QFont font;
        font.setPointSize(16);
        ValueBox->setFont(font);
        ValueBox->setStyleSheet(QLatin1String("color: rgb(234, 255, 2);\n"
"background-color: rgb(0, 0, 0);"));
        widget = new QWidget(ValueBox);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(10, 40, 211, 141));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        lab_rax = new QLabel(widget);
        lab_rax->setObjectName(QStringLiteral("lab_rax"));
        QFont font1;
        font1.setPointSize(14);
        lab_rax->setFont(font1);
        lab_rax->setScaledContents(false);

        verticalLayout->addWidget(lab_rax);

        lab_ray = new QLabel(widget);
        lab_ray->setObjectName(QStringLiteral("lab_ray"));
        lab_ray->setFont(font1);
        lab_ray->setScaledContents(true);

        verticalLayout->addWidget(lab_ray);

        lab_raz = new QLabel(widget);
        lab_raz->setObjectName(QStringLiteral("lab_raz"));
        lab_raz->setFont(font1);
        lab_raz->setScaledContents(true);

        verticalLayout->addWidget(lab_raz);

        lab_rgx = new QLabel(widget);
        lab_rgx->setObjectName(QStringLiteral("lab_rgx"));
        lab_rgx->setFont(font1);
        lab_rgx->setScaledContents(true);

        verticalLayout->addWidget(lab_rgx);

        lab_rgy = new QLabel(widget);
        lab_rgy->setObjectName(QStringLiteral("lab_rgy"));
        lab_rgy->setFont(font1);
        lab_rgy->setScaledContents(true);

        verticalLayout->addWidget(lab_rgy);

        lab_rgz = new QLabel(widget);
        lab_rgz->setObjectName(QStringLiteral("lab_rgz"));
        lab_rgz->setFont(font1);
        lab_rgz->setScaledContents(true);

        verticalLayout->addWidget(lab_rgz);

        layoutWidget = new QWidget(ValueBox);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(230, 40, 211, 141));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        lab_rang_x = new QLabel(layoutWidget);
        lab_rang_x->setObjectName(QStringLiteral("lab_rang_x"));
        lab_rang_x->setFont(font1);
        lab_rang_x->setScaledContents(false);

        verticalLayout_2->addWidget(lab_rang_x);

        lab_rang_y = new QLabel(layoutWidget);
        lab_rang_y->setObjectName(QStringLiteral("lab_rang_y"));
        lab_rang_y->setFont(font1);
        lab_rang_y->setScaledContents(true);

        verticalLayout_2->addWidget(lab_rang_y);

        lab_rang_z = new QLabel(layoutWidget);
        lab_rang_z->setObjectName(QStringLiteral("lab_rang_z"));
        lab_rang_z->setFont(font1);
        lab_rang_z->setScaledContents(true);

        verticalLayout_2->addWidget(lab_rang_z);

        lab_ang_x = new QLabel(layoutWidget);
        lab_ang_x->setObjectName(QStringLiteral("lab_ang_x"));
        lab_ang_x->setFont(font1);
        lab_ang_x->setScaledContents(true);

        verticalLayout_2->addWidget(lab_ang_x);

        lab_ang_y = new QLabel(layoutWidget);
        lab_ang_y->setObjectName(QStringLiteral("lab_ang_y"));
        lab_ang_y->setFont(font1);
        lab_ang_y->setScaledContents(true);

        verticalLayout_2->addWidget(lab_ang_y);

        lab_ang_z = new QLabel(layoutWidget);
        lab_ang_z->setObjectName(QStringLiteral("lab_ang_z"));
        lab_ang_z->setFont(font1);
        lab_ang_z->setScaledContents(true);

        verticalLayout_2->addWidget(lab_ang_z);

        layoutWidget_2 = new QWidget(ValueBox);
        layoutWidget_2->setObjectName(QStringLiteral("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(450, 40, 211, 176));
        verticalLayout_3 = new QVBoxLayout(layoutWidget_2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        lab_pwml = new QLabel(layoutWidget_2);
        lab_pwml->setObjectName(QStringLiteral("lab_pwml"));
        lab_pwml->setFont(font1);
        lab_pwml->setScaledContents(false);

        verticalLayout_3->addWidget(lab_pwml);

        lab_pwmr = new QLabel(layoutWidget_2);
        lab_pwmr->setObjectName(QStringLiteral("lab_pwmr"));
        lab_pwmr->setFont(font1);
        lab_pwmr->setScaledContents(true);

        verticalLayout_3->addWidget(lab_pwmr);

        lab_rang_z_2 = new QLabel(layoutWidget_2);
        lab_rang_z_2->setObjectName(QStringLiteral("lab_rang_z_2"));
        lab_rang_z_2->setFont(font1);
        lab_rang_z_2->setScaledContents(true);

        verticalLayout_3->addWidget(lab_rang_z_2);

        lab_null = new QLabel(layoutWidget_2);
        lab_null->setObjectName(QStringLiteral("lab_null"));
        lab_null->setFont(font1);
        lab_null->setScaledContents(true);

        verticalLayout_3->addWidget(lab_null);

        lab_ang_y_2 = new QLabel(layoutWidget_2);
        lab_ang_y_2->setObjectName(QStringLiteral("lab_ang_y_2"));
        lab_ang_y_2->setFont(font1);
        lab_ang_y_2->setScaledContents(true);

        verticalLayout_3->addWidget(lab_ang_y_2);

        lab_ang_z_2 = new QLabel(layoutWidget_2);
        lab_ang_z_2->setObjectName(QStringLiteral("lab_ang_z_2"));
        lab_ang_z_2->setFont(font1);
        lab_ang_z_2->setScaledContents(true);

        verticalLayout_3->addWidget(lab_ang_z_2);

        lab_rax->raise();
        lab_ray->raise();
        lab_raz->raise();
        lab_rgx->raise();
        lab_rgy->raise();
        lab_rgz->raise();
        lab_rgx->raise();
        layoutWidget->raise();
        layoutWidget_2->raise();
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        ValueBox->setTitle(QApplication::translate("MainWindow", "\346\225\260\345\200\274", 0));
        lab_rax->setText(QApplication::translate("MainWindow", "raw_ax:", 0));
        lab_ray->setText(QApplication::translate("MainWindow", "raw_ay:", 0));
        lab_raz->setText(QApplication::translate("MainWindow", "raw_az:", 0));
        lab_rgx->setText(QApplication::translate("MainWindow", "raw_gx:", 0));
        lab_rgy->setText(QApplication::translate("MainWindow", "raw_gy:", 0));
        lab_rgz->setText(QApplication::translate("MainWindow", "raw_gz:", 0));
        lab_rang_x->setText(QApplication::translate("MainWindow", "rAngle_x:", 0));
        lab_rang_y->setText(QApplication::translate("MainWindow", "rAngle_y:", 0));
        lab_rang_z->setText(QApplication::translate("MainWindow", "rAngle_z:", 0));
        lab_ang_x->setText(QApplication::translate("MainWindow", "Angle_x:", 0));
        lab_ang_y->setText(QApplication::translate("MainWindow", "Angle_y:", 0));
        lab_ang_z->setText(QApplication::translate("MainWindow", "Angle_z:", 0));
        lab_pwml->setText(QApplication::translate("MainWindow", "PWM_L:", 0));
        lab_pwmr->setText(QApplication::translate("MainWindow", "PWM_R:", 0));
        lab_rang_z_2->setText(QApplication::translate("MainWindow", "NULL", 0));
        lab_null->setText(QApplication::translate("MainWindow", "NULL", 0));
        lab_ang_y_2->setText(QApplication::translate("MainWindow", "NULL", 0));
        lab_ang_z_2->setText(QApplication::translate("MainWindow", "NULL", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
