//////////////////////////////////////////////
// 龙衡平衡小车tb6612 电机模块驱动
// 作者：zz                               
// Email:90807@QQ.com
// 本软件免费开源，修改请保留作者信息，谢谢
/////////////////////////////////////////////
#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/pwm.h>
#include <linux/fs.h>
#include <linux/gpio.h>

struct pwm_device * motoR;
struct pwm_device * motoL;

struct class *cls;

int open(struct inode * inode,struct file *fl)
{
  return 0;
}

long ioctl(struct file *fl, unsigned int cmd, unsigned long arg)
{
  int ret=0;
  switch(cmd)
 {
    case 0: //命令0左轮向前
         gpio_direction_output(46,0);
         gpio_direction_output(47,1);
         pwm_disable(motoL);
         ret=pwm_config(motoL,arg,20*1000*1000);
         ret=pwm_enable(motoL);
         break;
    case 1: //命令1左轮向后
         gpio_direction_output(46,1);
         gpio_direction_output(47,0);
         pwm_disable(motoL);
         ret=pwm_config(motoL,arg,20*1000*1000);
         ret=pwm_enable(motoL);
         break;
    case 2: //命令2右轮向前
         gpio_direction_output(48,0);
         gpio_direction_output(49,1);
         pwm_disable(motoR);
         ret=pwm_config(motoR,arg,20*1000*1000);
         ret=pwm_enable(motoR);
         break;
    case 3: //命令3右轮向后
         gpio_direction_output(48,1);
         gpio_direction_output(49,0);
         pwm_disable(motoR);
         ret=pwm_config(motoR,arg,20*1000*1000);
         ret=pwm_enable(motoR);
         break;
  } //end switch
  return ret;
}


struct file_operations fops={
	.owner=THIS_MODULE,
	.open=open,
	.unlocked_ioctl=ioctl,
};

int probe(struct platform_device *tb6612_dev)
{
  int ret;
  //创建设备文件
  cls=class_create(THIS_MODULE,"tb6612");
  ret=register_chrdev(234,"tb6612",&fops);
  device_create(cls,NULL,MKDEV(234,0),NULL,"tb6612");   
  
  //调整pwm初始值
  motoL=pwm_request(0,"motoL");
  motoR=pwm_request(1,"motoR");
  pwm_disable(motoL);
  pwm_disable(motoR);
  gpio_direction_output(46,0);  //motoL 使用gpio 46
  gpio_direction_output(47,0);  //motoL 使用gpio 47
  gpio_direction_output(48,0);  //motoR 使用gpio 46
  gpio_direction_output(49,0);  //motoR 使用gpio 47
  gpio_direction_output(50,1);  //驱动模块使能 使用gpio 50

  ret=pwm_config(motoL,0,20*1000*1000);
  ret=pwm_config(motoR,0,20*1000*1000);
  ret=pwm_enable(motoL);
  ret=pwm_enable(motoR);

  printk("TB6612 device probe!\n");
  printk("Power by zz <90807@qq.com>\n");
  return ret;
}

int remove(struct platform_device *tb6612_dev)
{
  //释放pwm资源
  pwm_disable(motoL);
  pwm_free(motoL);
  pwm_disable(motoR);
  pwm_free(motoR);
  gpio_direction_output(50,0);  //驱动模块使能 使用gpio 50
  //删除设备文件
  unregister_chrdev(234,"tb6612");
  device_destroy(cls,MKDEV(234,0));
  class_destroy(cls);
  printk("tb6612 device remove!\n");
  return 0;
}

//tb6612设备结构体
struct platform_device tb6612_dev={
	.name="tb6612",
	.id=0,
};

struct platform_device_id ids[]={
	{"tb6612"},
	{},
};

//tb6612驱动结构体
struct platform_driver tb6612_drv={
	.driver={
		.name="tb6612",
	},
	.probe=probe,
	.remove=remove,
	.id_table=ids,
};


int __init tb6612_init(void)
{
  int ret;

  ret=platform_device_register(&tb6612_dev);
  ret=platform_driver_register(&tb6612_drv);
  return ret;
}


void __exit tb6612_exit(void)
{

  platform_driver_unregister(&tb6612_drv);
  platform_device_unregister(&tb6612_dev);
}

module_init(tb6612_init);
module_exit(tb6612_exit);
MODULE_LICENSE("GPL");
