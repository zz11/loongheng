#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xaa11f9d6, "module_layout" },
	{ 0xccd334d3, "pwm_request" },
	{ 0x25b1b02e, "device_create" },
	{ 0x261c850b, "__register_chrdev" },
	{ 0xedd88526, "__class_create" },
	{ 0x6e854b5b, "pwm_enable" },
	{ 0xc5d5a182, "pwm_config" },
	{ 0x27e1a049, "printk" },
	{ 0xc3174b9e, "class_destroy" },
	{ 0x9c0140d5, "device_destroy" },
	{ 0x6bc3fbc0, "__unregister_chrdev" },
	{ 0xa8f59416, "gpio_direction_output" },
	{ 0xb764f9c8, "pwm_free" },
	{ 0xdaadf8b8, "pwm_disable" },
	{ 0x3a87654b, "platform_driver_register" },
	{ 0x4009494, "platform_device_register" },
	{ 0x1fe34f40, "platform_device_unregister" },
	{ 0x9cd97062, "platform_driver_unregister" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "94F83CD386DBFD50B587054");
