#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc,char * argv[])
{
  int fd,ret,i;
  if(argc<3)
  {
   printf("tb6612_test num speed\n");
   return -1;
  }
  
  fd=open("/dev/tb6612",O_RDWR);
  if (fd<0)
  {
   printf("tb6612 device open error\n");
   return -1;
  }

  ret=ioctl(fd,atoi(argv[1]),atoi(argv[2])*100*1000);

  return ret;
}


