#include "show.h"
#include "MiniBalance.h"
/**************************************************************************
作者：Mini Balance 
淘宝店铺：http://shop114407458.taobao.com/
**************************************************************************/
unsigned char i;          //计数变量
unsigned char Send_Count; //串口需要发送的数据个数
/**************************************************************************
函数功能：OLED显示
入口参数：无
返回  值：无
作    者：Mini Balance
**************************************************************************/
static u32 Count;
void oled_show(void)
{
	  Count=0;
		OLED_Display_On();  //显示屏打开

	
		//=============显示角度=======================//
		                      OLED_ShowString(0,40,"Angle");
		if(Pitch<0)		OLED_ShowNumber(45,40,Pitch+360,3,12);
		else					        OLED_ShowNumber(45,40,Pitch,3,12);
		//=============刷新=======================//
		//=============显示角度=======================//
		                      OLED_ShowString(0,50,"Angle");
		if(Roll<0)		OLED_ShowNumber(45,50,Roll+360,3,12);
		else					        OLED_ShowNumber(45,50,Roll,3,12);
		//=============刷新=======================//
		OLED_Refresh_Gram();	
	}
/**************************************************************************
函数功能：虚拟示波器往上位机发送数据 关闭显示屏
入口参数：无
返回  值：无
作    者：Mini Balance
**************************************************************************/
void DataScope(void)
{   
	  if(++Count==1)
		{	
		OLED_Clear();  
		OLED_Display_Off();		
		}	
		DataScope_Get_Channel_Data( Roll, 1 );
		DataScope_Get_Channel_Data( Pitch, 2 );
		DataScope_Get_Channel_Data( 0, 3 ); 
		DataScope_Get_Channel_Data( 0 , 4 );   
		DataScope_Get_Channel_Data(0, 5 ); //用您要显示的数据替换0就行了
		DataScope_Get_Channel_Data(0 , 6 );//用您要显示的数据替换0就行了
		DataScope_Get_Channel_Data(0, 7 );
		DataScope_Get_Channel_Data( 0, 8 ); 
		DataScope_Get_Channel_Data(0, 9 );  
		DataScope_Get_Channel_Data( 0 , 10);
		Send_Count = DataScope_Data_Generate(10);
		for( i = 0 ; i < Send_Count; i++) 
		{
		while((USART1->SR&0X40)==0);  
		USART1->DR = DataScope_OutPut_Buffer[i]; 
		}
		delay_ms(50); //20HZ
}
