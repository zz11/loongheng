#include "timer.h"
#include "mpu6050.h"
#include "led.h"
#include "math.h"
/**************************************************************************
作者：Mini Balance 
淘宝店铺：http://shop114407458.taobao.com/
**************************************************************************/
void  StartModule() 		         //启动模块
  {
  PAout(7)=1;
	delay_us(15);  
	PAout(7)=0;	
  }
/**************************************************************************
函数功能：绝对值函数
入口参数：int
返回  值：unsigned int
作    者：Mini Balance
**************************************************************************/
int myabs(int a)
{ 		   
	  int temp;
		if(a<0)  temp=-a;  
	  else temp=a;
	  return temp;
}

//psc：时钟预分频数
void TIM3_Cap_Init(u16 arr,u16 psc)	
{	
	u32 temp;	
	u8 IPROFFSET=TIM3_IRQChannel%4;//在组内的偏移	
	u32 tp,tp1;		 
	RCC->APB1ENR|=1<<1;   	//TIM2 时钟使能 
	RCC->APB2ENR|=1<<2;    	//使能PORTA时钟   	 
	GPIOA->CRL&=0X0FFFFFFF; 
	GPIOA->CRL|=0X30000000;//Pa.7 推挽输出   	 
  GPIOA->ODR|=1<<7;      //Pa.7 输出高	 
	GPIOA->CRL&=0XF0FFFFFF;	//PA0 清除之前设置  
	GPIOA->CRL|=0X08000000;	//PA0 输入   
	GPIOA->ODR|=0<<6;		//PA0 下拉	  
 	TIM3->ARR=arr;  		//设定计数器自动重装值   
	TIM3->PSC=psc;  		//预分频器 
	TIM3->CCMR1|=1<<0;		//CC1S=01 	选择输入端 IC1映射到TI1上
 	TIM3->CCMR1|=0<<4; 		//IC1F=0000 配置输入滤波器 不滤波
 	TIM3->CCMR1|=0<<10; 	//IC2PS=00 	配置输入分频,不分频 
	TIM3->CCER|=0<<1; 		//CC1P=0	上升沿捕获
	TIM3->CCER|=1<<0; 		//CC1E=1 	允许捕获计数器的值到捕获寄存器中
	TIM3->DIER|=1<<1;   	//允许捕获中断				
	TIM3->DIER|=1<<0;   	//允许更新中断	
	TIM3->CR1|=0x01;    	//使能定时器2		
	IPROFFSET=IPROFFSET*8+4;    //得到偏移的确切位置
	tp1=(~2)&0x07;//取后三位
	tp1<<=8;
	tp=SCB->AIRCR;  //读取先前的设置
	tp&=0X0000F8FF; //清空先前分组
	tp|=0X05FA0000; //写入钥匙
	tp|=tp1;	   
	SCB->AIRCR=tp;  //设置分组
	temp=1<<(4-2);	  
	temp|=3&(0x0f>>2);
	temp&=0xf;//取低四位

NVIC->ISER[0]|=1<<TIM3_IRQChannel;//使能中断位(要清除的话,相反操作就OK)
}
//捕获状态
//[7]:0,没有成功的捕获;1,成功捕获到一次.
//[6]:0,还没捕获到高电平;1,已经捕获到高电平了.
//[5:0]:捕获高电平后溢出的次数
u8  TIM2CH1_CAPTURE_STA=0;	//输入捕获状态		    				
u16	TIM2CH1_CAPTURE_VAL;	//输入捕获值
//定时器3中断服务程序	 
void TIM3_IRQHandler(void)
{ 		    
	u16 tsr;
	tsr=TIM3->SR;
 	if((TIM2CH1_CAPTURE_STA&0X80)==0)//还未成功捕获	
	{
		if(tsr&0X01)//溢出
		{	    
			if(TIM2CH1_CAPTURE_STA&0X40)//已经捕获到高电平了
			{
				if((TIM2CH1_CAPTURE_STA&0X3F)==0X3F)//高电平太长了
				{
					TIM2CH1_CAPTURE_STA|=0X80;//标记成功捕获了一次
					TIM2CH1_CAPTURE_VAL=0XFFFF;
				}else TIM2CH1_CAPTURE_STA++;
			}	 
		}
		if(tsr&0x02)//捕获1发生捕获事件
		{	
			if(TIM2CH1_CAPTURE_STA&0X40)		//捕获到一个下降沿 		
			{	  			
				TIM2CH1_CAPTURE_STA|=0X80;		//标记成功捕获到一次高电平脉宽
			    TIM2CH1_CAPTURE_VAL=TIM3->CCR1;	//获取当前的捕获值.
	 			TIM3->CCER&=~(1<<1);			//CC1P=0 设置为上升沿捕获
			}else  								//还未开始,第一次捕获上升沿
			{
				TIM2CH1_CAPTURE_STA=0;			//清空
				TIM2CH1_CAPTURE_VAL=0;
				TIM2CH1_CAPTURE_STA|=0X40;		//标记捕获到了上升沿
	 			TIM3->CNT=0;					//计数器清空
	 			TIM3->CCER|=1<<1; 				//CC1P=1 设置为下降沿捕获
			}		    
		}			     	    					   
 	}
	TIM3->SR=0;//清除中断标志位 	    
}

/**************************************************************************
函数功能：定时中断初始化
入口参数：arr：自动重装值  psc：时钟预分频数 
返回  值：无
作    者：Mini Balance
**************************************************************************/
void Timer1_Init(u16 arr,u16 psc)  
{  
	RCC->APB2ENR|=1<<11;//TIM2时钟使能    
 	TIM1->ARR=arr;  //设定计数器自动重装值//刚好1ms    
	TIM1->PSC=psc;  //预分频器7200,得到10Khz的计数时钟
	TIM1->DIER|=1<<0;   //允许更新中断				
	TIM1->DIER|=1<<6;   //允许触发中断	   
	TIM1->CR1|=0x01;    //使能定时器
	MY_NVIC_Init(1,3,TIM1_UP_IRQChannel,1);
}  

/**************************************************************************
函数功能：PWM 以及电机控制的IO初始化
入口参数：arr：自动重装值  psc：时钟预分频数 
返回  值：无
作    者：Mini Balance
**************************************************************************/
void MiniBalance_PWM_Init(u16 arr,u16 psc)
{		 					 
		//此部分需手动修改IO口设置
	RCC->APB1ENR|=1<<0;       //TIM2时钟使能    
	RCC->APB2ENR|=1<<2;       //PORTA时钟使能   

	GPIOA->CRL&=0XFFFF0000;//PB0输出
	GPIOA->CRL|=0X0000BBBB;//复用功能输出 	  

	TIM2->ARR=arr;//设定计数器自动重装值 
	TIM2->PSC=psc;//预分频器不分频
	
	TIM2->CCMR1|=7<<12;  //CH2 PWM2模式	
	TIM2->CCMR1|=7<<4;  //CH1 PWM2模式	
	TIM2->CCMR2|=7<<12;  //CH4 PWM2模式	
	TIM2->CCMR2|=7<<4;  //CH3 PWM2模式	 

	TIM2->CCMR1|=1<<11; //CH2预装载使能	 
	TIM2->CCMR1|=1<<3; //CH1预装载使能	  
	TIM2->CCMR2|=1<<11; //CH4预装载使能	 
	TIM2->CCMR2|=1<<3; //CH3预装载使能	 

	TIM2->CCER|=1<<4;   //OC2 输出使能	   
	TIM2->CCER|=1;   //OC2 输出使能	
	TIM2->CCER|=1<<12;   //OC2 输出使能	   
	TIM2->CCER|=1<<8;   //OC2 输出使能	

	TIM2->CR1=0x8000;   //ARPE使能 
	TIM2->CR1|=0x01;    //使能定时器2 										  							  
} 

void MY_NVIC_Init(u8 NVIC_PreemptionPriority,u8 NVIC_SubPriority,u8 NVIC_Channel,u8 NVIC_Group)	 
{ 
	u32 temp;	
	u8 IPROFFSET=NVIC_Channel%4;//在组内的偏移
	IPROFFSET=IPROFFSET*8+4;    //得到偏移的确切位置
	MY_NVIC_PriorityGroupConfig(NVIC_Group);//设置分组
	temp=NVIC_PreemptionPriority<<(4-NVIC_Group);	  
	temp|=NVIC_SubPriority&(0x0f>>NVIC_Group);
	temp&=0xf;//取低四位
	if(NVIC_Channel<32)NVIC->ISER[0]|=1<<NVIC_Channel;//使能中断位(要清除的话,相反操作就OK)
	else NVIC->ISER[1]|=1<<(NVIC_Channel-32);       	    	  				   
}
void MY_NVIC_PriorityGroupConfig(u8 NVIC_Group)	 
{ 
	u32 temp,temp1;	  
	temp1=(~NVIC_Group)&0x07;//取后三位
	temp1<<=8;
	temp=SCB->AIRCR;  //读取先前的设置
	temp&=0X0000F8FF; //清空先前分组
	temp|=0X05FA0000; //写入钥匙
	temp|=temp1;	   
	SCB->AIRCR=temp;  //设置分组	    	  				   
}
